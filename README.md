# Corroded Chess

## Team Members:
 * Mark Wignall <mwignall@pdx.edu>
 * Camilo Schaser-Hugtes <camilo3@pdx.edu>

## Objective:
 * For this application, we intend to deliver an integrated web application which presents players to play a game of chess on their web browser.
 * Fulfill the requirements in conjunction with developing an application written in the Rust programming language, as outlined in the project assignment.

 ## Organization:
 * `corroded_chess`
    * Rust Library
    * Does the math.
 * `corroded_web`
    * Rust Binary
    * Does the input / Output.

 