# CS410P-Programming Rust

Final Project
Author: Mark Wignall
Date: 03.14.2021 (Winter 2022)

## Overview

This program project was a joint effort by Camillo Camilo Schaser-Hugtes and my self to hilight and showcase the
fundemental Rust concepts learned thought the term.

Effectively, the two crates created were a backend library, and front end web application that uses the library.

The Front end crate, was _NOT_ meant to be a full blown web chess application between remote players. Rather, it
was intended to be a thin wrapper web application so that players could play locally, eleminating extra research and scope for this project. The crux of this crate was meant to interact with the chess library crate, and manipulate a PNG file based on the moves made, and encode the moves with the PNG's meta data.

The library crate, as mentioned was intended to _NOT_ maintain states itself, but rather rely heavily on &self references so that users of the library would persist and maintain any variables that were created from this libraries types.

## Invocation

This package is comprised of both the library crate and a wrapper excecutable crate that can be used to interact with the library, and has two binaries available. Please see the README file for the _command_web_ crate, for instructions on how to invoke the web client application.

However, during development, and intending to work independantly, I created a binary that uses the command line
to instanciate a game and interact with the methods for testing purposes.

To use this cmd line verion, you can enter the following from the root crate's directory
`cargo run -p corroded-chess`

## Developer Notes

largely, this effort was done in isolation, and only required a few online meetings in order to establish the
methods and how to use them.

As part of the documentation and testing requirements for this project, there is a lot of example text in the
comments that is used during testing as in:

`cargo test -p corroded-chess`

Which not only produce the successfull passes, but also serves as documentation that users of the library can reference

As far as challenges went, there were plenty. Initial efforts were VERY manual and a lot of repeated code etc. As the course continued to progress and introduce new concepts, the desire to implement those forced alot of refactoring to implent them. Moreover, other concepts that were lightly touced upon, such as crates and modules, required alot of researching based on how are project was organized. E.g. Using workspacesa and having a default binary that runs from the top level crate.

Another example that was both rewarding and challenging was the creation of macros to help reduce the repetive code, and required re-learning that concept and moreso, on how to create one

In the end, the core of the libary is implemented, however, ONLY Pawn pieces are eligible to move at this time, as the logic for doing so was very much hardcoded, and started to get that feeling of, this is NOT rust like, and must have a better way to do so. Rather than having non-Rustacean code, that implememted features, I felt it more important to showcase the rust concepts and limit the scope of the libary for now.

I fully plan on continuing to evolve this libary, even after the deadline, however, will not be refreshing the git lab for fear of messing up the grading process.

Had a blast doing this project, and can't wait to continue!
