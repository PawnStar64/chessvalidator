use corroded_chess::engine::Game;

fn main() {
    let mut game = Game::new();

    println!("Game: {:?}", game);
    println!("Game Encoded: {:?}", game.encode_board());
    println!("Color who's turn it is: {:?}", game.get_turn_color());

    // Not a real location
    match game.make_move("z9") {
        Ok(value) => println!("Success {:?}", value),
        Err(e) => println!("Failure {:?}", e),
    }
    // Should be ambiguous
    match game.make_move("a4") {
        Ok(value) => println!("Success {:?}", value),
        Err(e) => println!("Failure {:?}", e),
    }
    println!("Game: {:?}", game);

    match game.make_move("b5") {
        Ok(value) => println!("Success {:?}", value),
        Err(e) => println!("Failure {:?}", e),
    }
    println!("Game: {:?}", game);

    // // Sould be legit
    // match game.make_move("Rxa3+") {
    //     Ok(value) => println!("Success {:?}", value),
    //     Err(e) => println!("Failure {:?}", e),
    //}
}
