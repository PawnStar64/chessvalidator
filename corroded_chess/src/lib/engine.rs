use crate::{
    errors::{self, *},
    pieces::{self, *},
};
use regex::Regex;
use std::{collections::HashMap, fmt};

const FILE_LABELS: [char; 8] = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'];
const RANK_LABELS: [char; 8] = ['1', '2', '3', '4', '5', '6', '7', '8'];

#[derive(Debug)]
pub struct Square {
    file: u32,
    rank: u32,
}

impl Square {
    pub fn from_text(s: &str) -> Square {
        let chars: Vec<char> = s.to_ascii_lowercase().chars().collect();

        Square {
            file: chars[0] as u32 - 'a' as u32,
            rank: chars[1] as u32 - '1' as u32,
        }
    }

    pub fn to_text(&self) -> String {
        let mut s = String::from("");
        s.push((self.file as u8 + b'a') as char);
        s.push((self.rank as u8 + b'1') as char);
        s
    }
}

/// Game type which contains all the game's internal state information and provides
/// methods to interact with the Game, namely making moves
#[derive(Clone)]
pub struct Game<'a> {
    move_history: Vec<String>, // Historical list of moves taken during the game
    board_map: HashMap<String, &'a Piece>, // Contains only pieces actually on the board
}

/// Default implementation to allow for instanciate without any parameters
impl Default for Game<'_> {
    fn default() -> Self {
        Self::new()
    }
}

impl Game<'_> {
    /// Internal method for reseting the *internal state* of the Game as well as setting
    /// the chess pieces to their initial configuration
    ///
    /// All previously recorded moves will be deleted and the inital pieces for each
    /// player color will be placed in their starting positions
    ///
    /// # Examples
    /// ```
    ///     use corroded_chess::engine::Game;
    ///
    ///     let game = Game::new();
    /// ```
    pub fn new() -> Self {
        let mut temp = Game {
            move_history: Vec::new(),
            board_map: HashMap::new(),
        };
        Game::reset_board(&mut temp);
        temp
    }
    /// Returns the Color of the player who's turn it is
    ///
    /// Since White always starts a game of Chess, it should always be an odd index into
    /// the running list of moves completed.
    ///
    /// When nomenclated in Chess's Algebraic Notation, *moves* are nomenclated by a move
    /// number, followed by the white players move and the black players moves as complete pairs.
    ///
    /// For example, Turn 1, may have one of the following entries
    /// 1. e4           (Only White players move completed, so next turn is the black player)        
    /// or
    /// 1. e4 e5        (White and black have both completed turn #1)
    ///
    /// # Examples
    /// ```
    ///     use corroded_chess::{engine::Game, pieces::Color};
    ///
    ///     assert_eq!(Color::White, Game::new().get_turn_color());
    /// ```
    pub fn get_turn_color(&self) -> Color {
        match self.move_history.len() % 2 {
            0 => Color::White,
            _ => Color::Black,
        }
    }
    /// Resets the corresponding game's board to the initial starting positions of the chess pieces
    ///
    /// This method is called during the contruction of the Game type and can also be
    /// used afterwards to reset the game back to its initial configuration
    ///
    /// # Examples
    /// ```
    ///     use corroded_chess::*;
    ///
    ///     let mut game: engine::Game = engine::Game::default();     // Implicitly calls rest_board()
    ///     game.reset_board();
    /// ```
    pub fn reset_board(&mut self) {
        self.move_history.clear();
        self.board_map.clear();

        // Fill in White's back row with starting pieces
        self.board_map.insert("a1".to_string(), &WHITE_ROOK);
        self.board_map.insert("b1".to_string(), &WHITE_KNIGHT);
        self.board_map.insert("c1".to_string(), &WHITE_BISHOP);
        self.board_map.insert("d1".to_string(), &WHITE_QUEEN);
        self.board_map.insert("e1".to_string(), &WHITE_KING);
        self.board_map.insert("f1".to_string(), &WHITE_BISHOP);
        self.board_map.insert("g1".to_string(), &WHITE_KNIGHT);
        self.board_map.insert("h1".to_string(), &WHITE_ROOK);

        // Fill in Black's back row with starting pieces
        self.board_map.insert("a8".to_string(), &BLACK_ROOK);
        self.board_map.insert("b8".to_string(), &BLACK_KNIGHT);
        self.board_map.insert("c8".to_string(), &BLACK_BISHOP);
        self.board_map.insert("d8".to_string(), &BLACK_QUEEN);
        self.board_map.insert("e8".to_string(), &BLACK_KING);
        self.board_map.insert("f8".to_string(), &BLACK_BISHOP);
        self.board_map.insert("g8".to_string(), &BLACK_KNIGHT);
        self.board_map.insert("h8".to_string(), &BLACK_ROOK);

        // Next the rank's of Pawns
        for file in FILE_LABELS {
            let mut wsquare: String = file.to_string();
            wsquare.push(RANK_LABELS[1]);
            let bsquare = wsquare.replace(RANK_LABELS[1], RANK_LABELS[6].to_string().as_str());

            self.board_map.insert(wsquare, &WHITE_PAWN);
            self.board_map.insert(bsquare, &BLACK_PAWN);
        }
    }

    /// Helper function to determine if a nomenclated rank/file location on the board is a valid square.
    ///
    /// To determine if it is valid, the string passed in must have a length of two characters and the first character is treated as the *File* for Chess'
    /// [Algebraic Notation](https://en.wikipedia.org/wiki/Algebraic_notation_(chess)), followed by the *rank*
    ///
    /// The resulting set of valid locations then, are:      
    /// ![Chess Board Locations](https://www.chessable.com/blog/wp-content/uploads/2021/09/Algebraic-Notation-300x300.png)
    ///
    /// # Examples
    ///
    /// ```
    ///     use corroded_chess::engine::Game;
    ///
    ///     let game = Game::new();
    ///
    ///     assert_eq!(true, Game::valid_location("a8"));
    ///     assert_eq!(false, Game::valid_location("z9"));
    /// ```
    pub fn valid_location(square: &str) -> bool {
        let mut ch = square.chars();
        let dest_file = ch.next();
        let dest_rank = ch.next();

        if square.len() != 2
            || (!FILE_LABELS.iter().any(|&file| file == dest_file.unwrap())
                && !RANK_LABELS.iter().any(|&rank| rank == dest_rank.unwrap()))
        {
            return false;
        }
        true
    }

    /// Returns the chess Piece that is located on the requested square, or None if no piece was present
    ///
    /// # Examples
    /// ```
    ///     use corroded_chess::{engine, pieces, errors::ChessError};
    ///
    ///     let mut game: engine::Game = engine::Game::default();     // Implicitly calls rest_board()
    ///
    ///     assert_eq!(Ok(Some(&pieces::BLACK_ROOK)), game.get_piece("a8"));
    ///     assert_eq!(Ok(Some(&pieces::WHITE_ROOK)), game.get_piece("h1"));
    ///     assert_eq!(Ok(Some(&pieces::WHITE_PAWN)), game.get_piece("a2"));
    ///     assert_eq!(Ok(None), game.get_piece("a3"));
    ///     assert_eq!(Err(ChessError::InvalidSquareLocation), game.get_piece("z9"));
    /// ```
    pub fn get_piece(&self, square: &str) -> Result<Option<&Piece>, errors::ChessError> {
        if !Game::valid_location(square) {
            return Err(ChessError::InvalidSquareLocation);
        }

        // Since our HashMap contains reference to pieces, and any get() operation
        // will return a borrowed reference to that reference, we de-reference here
        // to hide the internal structure and merely return the same reference used
        // when adding it to the HashMap
        match self.board_map.get(square) {
            Some(value) => Ok(Some(value)),
            None => Ok(None),
        }
    }

    /// Encodes the current state of the board, depicting which piece is in which location
    /// into a String
    pub fn encode_board(&self) -> String {
        let mut s: String = String::with_capacity(FILE_LABELS.len() * RANK_LABELS.len());

        for rank in RANK_LABELS {
            for file in FILE_LABELS {
                let slice: &str = &format!("{}{}", file, rank);

                let z = match self.get_piece(slice) {
                    Ok(piece) => match piece {
                        Some(piece) => piece.to_char(),
                        None => '.',
                    },
                    _ => '?',
                };
                s.push(z);
            }
        }

        // Give back the newly encoded String
        s
    }

    /// Attempt to move a piece that is on the board to a desired location.
    ///
    /// The move requested, should follow the Chess Algebraic Notation
    pub fn make_move(
        &mut self,
        notation: &str,
    ) -> Result<(&Piece, String, String), errors::ChessError> {
        // Parse the notation into its constituents
        //
        // Match $1 = Type of piece (Knight = N, King=K, Queen=Q, Rook=R, Bishop=B, Pawn="")
        // Match $2 = Qualifing Rank (1-8) - Used only if square and file are ambiquous
        // Match $3 = Qualifing File (a-h) - Used only if square is ambiguous
        // Match $4 = Captured a piece (x) as a result of the move - Type captured is not specified
        // Match $5 = Square (a1, b1, ... g8, h8) - Destination/Target
        // Match $6 = Check (+) - If result of move placed the oppenent in check
        let notation_re = Regex::new(r"([RNBQK]?)([a-h]*)([1-8]*)(x?)([a-h][1-8])(\+?)").unwrap();
        let captures = notation_re.captures(notation);

        let piece_type: Kind;
        let rank: String;
        let file: String;
        let captured: bool;
        let square: String;
        let check: bool;

        println!(
            "\n\
                  ==================================\n\
                  Requested Move: {:?}",
            notation
        );

        match captures {
            None => return Err(errors::ChessError::InvalidNotation),
            Some(caps) => {
                piece_type = caps[1]
                    .to_ascii_uppercase()
                    .chars()
                    .next()
                    .map_or(Kind::Pawn, Kind::from_char);
                rank = caps[2]
                    .chars()
                    .next()
                    .map_or(String::new(), |c| c.to_string());
                file = caps[3]
                    .chars()
                    .next()
                    .map_or(String::new(), |c| c.to_string());
                captured = caps[4].chars().next().map_or(false, |_c| true);
                square = String::from(caps[5].chars().as_str());
                check = caps[6].chars().next().map_or(false, |_c| true);
            }
        }

        println!(
            "----------------------------------\n\
                  PieceType={:?}\n\
                  Rank'={:?}\n\
                  file'={:?}\n\
                  captured={:?}\n\
                  square={:?}\n\
                  check={:?}\n",
            piece_type, rank, file, captured, square, check
        );

        // Filter out all the pieces currently game that are of the Type that was indicated in the notation
        // as well as the color matching the current players color.
        let mut candidates = self.board_map.clone();
        candidates.retain(|_, v| v.kind == piece_type && v.color == self.get_turn_color());

        // For each candidate piece then, populate a list of their current squares which,
        // when moved, can reach the target square
        let mut _source_squares: Vec<(String, &Piece)> = Vec::new();

        for candidate in candidates {
            let _p = &candidate.1;
            let _s = &candidate.0;

            //println!("Evaluating target locations for piece {:?} {:?} Located at {:?}", p.color, p.Kind, s);

            match Game::piece_destinations(self, candidate.clone()) {
                Err(e) => return Err(e), // Propogate the err up
                Ok(originating_squares) => {
                    if originating_squares.contains(&square) {
                        _source_squares.push(candidate.clone());
                    }
                    println!(
                        "Existing pieces that can move to {:?} are: {:?}",
                        square, _source_squares
                    );
                }
            }
        }

        println!(
            "Found {:?} that can reach this square",
            _source_squares.len()
        );

        match _source_squares.len() {
            0 => Err(ChessError::InvalidSquareLocation),
            1 => {
                let piece = _source_squares.first().unwrap().clone();
                // Success move requested, so remove the old piece and add it's new location
                self.board_map.remove(&piece.0);
                self.board_map.insert(square.clone(), piece.1);
                self.move_history.push(notation.to_string());
                Ok((piece.1, piece.0, square))
            }
            _ => Err(ChessError::NotationAmbiguous),
        }
    }

    /// Produces a list of possible square destinations for a given piece and its starting
    /// location
    ///
    /// The first varient of the tuple suplied is the starting square location, specified as a
    /// String for which the first character is the File, and the second character is the Rank.
    ///
    /// The second varient is a reference to the type of piece
    pub fn piece_destinations(
        &self,
        candidate: (String, &Piece),
    ) -> Result<Vec<String>, errors::ChessError> {
        let mut reachable_squares: Vec<String> = Vec::new();

        let square = candidate.0;

        if !Game::valid_location(&square) {
            return Err(ChessError::InvalidSquareLocation);
        }

        if candidate.1 == &pieces::WHITE_PAWN {
            let mut r = Square::from_text(&square);
            let p = self.get_piece(&r.to_text()).unwrap_or(None);

            println!("Evaluating {:?} at location {:?}", p.unwrap(), square);

            // Check if can move up one spaces
            if r.rank < RANK_LABELS.len() as u32 {
                r.rank += 1;
                match self.get_piece(&r.to_text()).unwrap() {
                    Some(_p) => (),
                    None => {
                        reachable_squares.push(r.to_text());
                        println!(
                            "... Square [{:?}] was EMPTY, so adding to valid target squares",
                            &r.to_text(),
                        );
                    }
                }
            }
            // Check if can move up one spaces
            if r.rank < RANK_LABELS.len() as u32 {
                r.rank += 1;
                match self.get_piece(&r.to_text()).unwrap() {
                    Some(_p) => (),
                    None => {
                        reachable_squares.push(r.to_text());
                        println!(
                            "... Square [{:?}] was EMPTY, so adding to valid target squares",
                            &r.to_text(),
                        );
                    }
                }
            }
        }

        if candidate.1 == &pieces::BLACK_PAWN {
            let mut r = Square::from_text(&square);
            let p = self.get_piece(&r.to_text()).unwrap_or(None);

            println!("Evaluating {:?} at location {:?}", p.unwrap(), square);

            // Check if can move down one spaces
            if r.rank > 0_u32 {
                r.rank -= 1;
                match self.get_piece(&r.to_text()).unwrap() {
                    Some(_p) => (),
                    None => {
                        reachable_squares.push(r.to_text());
                        println!(
                            "... Square [{:?}] was EMPTY, so adding to valid target squares",
                            &r.to_text(),
                        );
                    }
                }
            }
            // Check if can move up one spaces
            if r.rank > 1_u32 {
                r.rank -= 1;
                match self.get_piece(&r.to_text()).unwrap() {
                    Some(_p) => (),
                    None => {
                        reachable_squares.push(r.to_text());
                        println!(
                            "... Square [{:?}] was EMPTY, so adding to valid target squares",
                            &r.to_text(),
                        );
                    }
                }
            }
        }

        Ok(reachable_squares)
    }
}

/// Implementation for Debug trait, namely the fmt method
///
/// This allows for the use of println! macro to output Game variables.
/// Usefull in debuging and/or logging.
///
/// # Examples
/// ```
///     use corroded_chess::engine::Game;
///
///     println!("Game: {:?}", Game::new());
/// ```
impl fmt::Debug for Game<'_> {
    /// Formatter for Game types
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut rev_rank = RANK_LABELS;
        rev_rank.reverse();

        writeln!(f)?;
        for rank in rev_rank {
            write!(f, "{:2}|", rank)?;
            for file in FILE_LABELS {
                let slice: &str = &format!("{}{}", file, rank);

                let p = self.get_piece(slice);

                let z = match p {
                    Ok(piece) => match piece {
                        Some(p) => p.to_char(),
                        None => '.',
                    },
                    _ => '?',
                };

                write!(f, "{:^3}", z)?
            }
            writeln!(f)?;
        }

        writeln!(f, "{:3}{}", "", "===".repeat(FILE_LABELS.len()))?;
        Ok(())
    }
}
