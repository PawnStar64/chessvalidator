use std::fmt;

#[derive(Debug, PartialEq)]
pub enum ChessError {
    InvalidSquareLocation,
    InvalidNotation,
    NotReachable,
    NotationAmbiguous,
}

impl std::error::Error for ChessError {}

impl fmt::Display for ChessError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ChessError::InvalidSquareLocation => write!(f, "Invalid Square Location"),
            ChessError::InvalidNotation => write!(f, "Invalid Chess Move Notation"),
            ChessError::NotReachable => write!(f, "Taget square unreachable by any piece"),
            ChessError::NotationAmbiguous => write!(f, "Taget square reachable by many pieces"),
        }
    }
}
