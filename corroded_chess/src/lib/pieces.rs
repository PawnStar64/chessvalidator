#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub enum Color {
    White,
    Black,
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub enum Kind {
    Pawn,
    Knight,
    Bishop,
    Rook,
    Queen,
    King,
}

impl Kind {
    pub fn from_char(c: char) -> Kind {
        match c.to_ascii_uppercase() {
            'K' => Kind::King,
            'Q' => Kind::Queen,
            'R' => Kind::Rook,
            'B' => Kind::Bishop,
            'N' => Kind::Knight,
            _ => Kind::Pawn,
        }
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub struct Piece {
    pub kind: Kind,
    pub color: Color,
    pub value: u8,
}

impl Piece {
    pub fn to_char(&self) -> char {
        let c = match self.kind {
            Kind::Pawn => 'P',
            Kind::Knight => 'N',
            Kind::Bishop => 'B',
            Kind::Rook => 'R',
            Kind::Queen => 'Q',
            Kind::King => 'K',
        };

        if self.color == Color::Black {
            c.to_lowercase().next().unwrap()
        } else {
            c
        }
    }
}

// Macro to ease the subsequent definitions for each piece type in the game of chess
macro_rules! make_piece {
    ($id:ident; $kind:expr; $color:expr; $value:expr) => {
        pub static $id: Piece = Piece {
            kind: $kind,
            color: $color,
            value: $value,
        };
    };
}

pub static TEST_PIECE: Piece = Piece {
    kind: Kind::Pawn,
    color: Color::White,
    value: 99,
};

// Make some publicly static pieces that can be referenced to
// external modules/crates, with defined (i.e. permanent) characteristics
// in order to prevent having to dynamically allocate pieces all the time from the heap
make_piece!(WHITE_KING; Kind::King; Color::White; 0);
make_piece!(WHITE_QUEEN; Kind::Queen; Color::White; 9);
make_piece!(WHITE_ROOK; Kind::Rook; Color::White; 5);
make_piece!(WHITE_BISHOP; Kind::Bishop; Color::White; 3);
make_piece!(WHITE_KNIGHT; Kind::Knight; Color::White; 3);
make_piece!(WHITE_PAWN; Kind::Pawn; Color::White; 1);

make_piece!(BLACK_KING; Kind::King; Color::Black; 0);
make_piece!(BLACK_QUEEN; Kind::Queen; Color::Black; 9);
make_piece!(BLACK_ROOK; Kind::Rook; Color::Black; 5);
make_piece!(BLACK_BISHOP; Kind::Bishop; Color::Black; 3);
make_piece!(BLACK_KNIGHT; Kind::Knight; Color::Black; 3);
make_piece!(BLACK_PAWN; Kind::Pawn; Color::Black; 1);
