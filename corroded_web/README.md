# Corroded Web

## Name: Camilo Schaser-Hughes
## Date: March 12, 2022
## Project for C510

### What I did:
 * built a webserver using actix-web
 * used handlebars to display 'dynamic' page
 * got a get and post site to cycle through moves
 * encoded text representation of board to png (later dropped)
 * built function to open, update and save image for display on web page (partially dropped).
 * built function to save game state in text file (later dropped)
 * created struct and mutex to have data persist on server
 * integrated the corroded chess into the corroded web crates.


### How it went:
It went ok.  Not great.  I was confused through a lot of it as to what the end product would be looking like.  Had trouble getting past the actix-web book example in understanding.  Found a tutorial about actix-web and kind of mashed that together with the book example.  Tried to learn WebSockets in relation to actix-web, but the examples from their git hub were a little beyond my understanding.  Also didn't ask for help when probably should have.  Overall a little disappointed in my end of the project.
Update, was able to find the example I needed for persistent states through pages in actix-web after revisiting documentation I inintially didn't fully understand and hand waved past.  So now much more satisfied with project.
Still wasn't able to figure out how to take a dynamic image and post it straight to the html page which would have made everything substantially quicker since I think most of the time was taken up on the opening and saving images, so if you have any recommended readings on that, that would be awesome.

### How I tested it:
Mostly just ran it and checked out the page that it produced.
Wasn't exactly sure how to run tests with images or actix web types.
Lots of print statements.