use image::{DynamicImage, GenericImage};

pub fn say_hello() -> &'static str {
    "hello world, first words!"
}

/// change_board takes in two string pointers and returns bool
/// two string represent two boards, redraws board rep by start
/// to board represented by finish.  loaded and saved from static path.
pub fn change_board(start: &str, finish: &str, img: &mut DynamicImage) -> bool {
    println!("starting change board function...");
    // let mut img = image::open("static/image/current.png").unwrap();
    let mut start_chars = start.chars();

    // asserts that both strings are formatted correctly.
    if start.len() != 64 || start.len() != finish.len() {
        return false;
    }

    // goes through both strings and changes board where is different.
    for (i, char) in finish.chars().enumerate() {
        if char == start_chars.next().unwrap() {
            continue;
        }
        let file_end = match char {
            '.' => {
                if (i / 8) % 2 == 1 {
                    if (i % 8) % 2 == 0 {
                        "wtile.png"
                    } else {
                        "btile.png"
                    }
                } else if (i % 8) % 2 == 0 {
                    "btile.png"
                } else {
                    "wtile.png"
                }
            }
            'p' => "bPawn.png",
            'P' => "wPawn.png",
            'r' => "bRook.png",
            'R' => "wRook.png",
            'n' => "bKnight.png",
            'N' => "wKnight.png",
            'b' => "bBishop.png",
            'B' => "wBishop.png",
            'q' => "bQueen.png",
            'Q' => "wQueen.png",
            'k' => "bKing.png",
            'K' => "wKing.png",
            something => {
                println!(
                    "some error key found somewhere somehow... it was {}",
                    something
                );
                return false;
            }
        };

        // determines the location to change the piece
        let col_pos: u32 = (48 + ((i % 8) * 162) + 31).try_into().unwrap();
        // had the board backwards originally, hence the weird 63-i here.
        let row_pos: u32 = (18 + (((63 - i) / 8) * 162) + 31).try_into().unwrap();

        // copies the specified piece onto the board
        let file_path = format!("static/image/piece/{}", file_end);
        let piece = image::open(file_path).unwrap();
        let _ = img.copy_from(&piece, col_pos, row_pos).unwrap();
    }

    img.save("static/image/current.png").unwrap();

    true
}

/// _create_board, takes in a string returns bool of success.
/// saves board to a statically designated location.
/// not used in current iteration of crate. but good to have around because I
/// don't know what the opening board of online chess looks like.
pub fn _create_board(input: &str) -> bool {
    let mut img = image::open("static/image/board.png").unwrap();
    let bp = image::open("static/image/piece/bPawn.png").unwrap();
    let wp = image::open("static/image/piece/wPawn.png").unwrap();

    // asserts that string is formatted correctly.
    if input.len() != 64 {
        return false;
    }

    for (i, char) in input.chars().enumerate() {
        if char == '.' {
            continue;
        }
        let col_pos: u32 = (48 + ((i % 8) * 162) + 31).try_into().unwrap();
        let row_pos: u32 = (18 + (((63 - i) / 8) * 162) + 31).try_into().unwrap();
        if char == 'p' {
            let _ = img.copy_from(&bp, col_pos, row_pos).unwrap();
            continue;
        } else if char == 'P' {
            let _ = img.copy_from(&wp, col_pos, row_pos).unwrap();
            continue;
        } else {
            let file_end = match char {
                'r' => "bRook.png",
                'R' => "wRook.png",
                'n' => "bKnight.png",
                'N' => "wKnight.png",
                'b' => "bBishop.png",
                'B' => "wBishop.png",
                'q' => "bQueen.png",
                'Q' => "wQueen.png",
                'k' => "bKing.png",
                'K' => "wKing.png",
                _ => {
                    return false;
                }
            };
            let file_path = format!("static/image/piece/{}", file_end);
            let piece = image::open(file_path).unwrap();
            let _ = img.copy_from(&piece, col_pos, row_pos).unwrap();
        }
    }
    // CAN'T HAVE / IN THE OPENING OF FILE... maybe ./ would work?
    img.save("static/image/opening.png").unwrap();
    // top = 18px, left = 48 pixels, squares=162^2, pieces=100^2
    true
}
