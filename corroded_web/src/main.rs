use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer};
/// Program to display a somewhat dynamic website.
/// Displays a chessboard on the page with pieces.  Allows the user to
/// enter in a piece, initial position and new position and will
/// update the board.  Currently is very slow for redoing image.
// Got the base for this from:
// https://dev.to/michaelin007/creating-a-web-page-with-actix-web-rust--2agd
use corroded_chess::engine::Game;
use handlebars::Handlebars;
use image::DynamicImage;
use serde::Deserialize;
use serde_json::json;

use core::str;
// use arc and mutex to pass the info safely through the pages.
use std::sync::{Arc, Mutex};
mod image_maker;

#[derive(Deserialize)]
struct MoveParameters {
    newpos: String,
}

// found help with the data moving between pages here:
// https://github.com/actix/actix-web/issues/874
#[derive(Debug)]
struct AppState<'a> {
    the_game: Game<'a>,
    game_state: String,
    game_img: DynamicImage,
}

/// initial page that you get when first visit and no moves have been made.
async fn init_index(
    hb: web::Data<Handlebars<'_>>,
    data: web::Data<Arc<Mutex<AppState<'_>>>>,
) -> HttpResponse {
    // this is just to test getting from the other file.
    let phrase = image_maker::say_hello();

    // hopefully this will be faster...
    let game = data.lock().unwrap();
    let img = &game.game_img;
    let turn = &game.the_game.get_turn_color();

    img.save("static/image/current.png").unwrap();

    // this is the info that will be passed into the page to make it dynamic.
    let json_data = json!({
        "project_name": "Corroded Chess",
        "image_path":"/static/image/opening.png",
        "success":"No moves made yet.",
        "your_move":phrase,
        "turn_col": format!("{:?}'s turn!", turn),
    });

    // assuming all went alright, returns the webpage.
    let body = hb.render("index", &json_data).unwrap();
    HttpResponse::Ok().body(body)
}

/// when web page it retrieved using a post as opposed to a get will
/// bring up this site, essentially the same thing but handles the input.
async fn index(
    hb: web::Data<Handlebars<'_>>,
    form: web::Form<MoveParameters>,
    data: web::Data<Arc<Mutex<AppState<'_>>>>,
) -> HttpResponse {
    let mov_att = form.newpos.clone();
    // println!("the data!!! {:?}", data);

    let mut game = data.lock().unwrap();
    let current = game.game_state.clone();

    // println!("current game state {}", &current);
    let turn = &game.the_game.get_turn_color();

    // this throws to the game and moves the piece.
    let new_res = game.the_game.make_move(&mov_att);

    // checks the validity of the move
    let (_piece, _source, _dest) = match new_res {
        // if invalid, posts the error and doesn't update anything.
        Err(any) => {
            println!("{:?}", any);
            let json_data = json!({
                "project_name": "Corroded Chess",
                "image_path":"/static/image/current.png",
                "success": format!("Unsuccessful!: {}", any),
                "your_move": format!("Your attempted move: {}", &mov_att),
                "turn_col": format!("Still {:?}'s turn!", turn),
            });

            let body = hb.render("index", &json_data).unwrap();
            return HttpResponse::Ok().body(body);
        }
        Ok((a_piece, some_source, some_dest)) => (a_piece, some_source, some_dest),
    };

    // get's the new board, or should.
    let newer = game.the_game.encode_board();
    // let newer = "RNBQKBNRPPP.PPPP...........P....................pppppppprnbqkbnr".to_string();

    // updates and then saves the board.
    if !image_maker::change_board(&current, &newer, &mut game.game_img) {
        println!("went wrong in the change board function");
        return HttpResponse::Ok().body("<h1>404</h1>");
    }

    // updates the board for the next iteration.
    game.game_state = newer;
    let turn = &game.the_game.get_turn_color();

    // moves data into the index page using the handlebars.
    let json_data = json!({
        "project_name": "Corroded Chess",
        "image_path":"/static/image/current.png",
        "success":"Successful Move! Great Job",
        "your_move": format!("You moved to {}", &mov_att,),
        "turn_col": format!("{:?}'s turn!", turn),
    });

    let body = hb.render("index", &json_data).unwrap();
    HttpResponse::Ok().body(body)
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // handlebars helps post data to the webpage.
    let mut handlebars = Handlebars::new();
    handlebars
        .register_templates_directory(".html", "./static/")
        .unwrap();
    let handlebars_ref = web::Data::new(handlebars);

    let data = Arc::new(Mutex::new(AppState {
        the_game: Game::new(),
        game_state: "RNBQKBNRPPPPPPPP................................pppppppprnbqkbnr".to_string(),
        game_img: image::open("static/image/opening.png").unwrap(),
    }));

    let server = HttpServer::new(move || {
        App::new()
            // handlebars helps to write the webpages.
            .app_data(handlebars_ref.clone())
            .data(data.clone())
            .service(Files::new("/static", "static").show_files_listing())
            // puts up the initial page
            .route("/", web::get().to(init_index))
            // when posts, changes the site.
            .route("/", web::post().to(index))
    });

    // allows easy click to site
    println!("Server listening on http://localhost:9090 .  How about that?");

    server.bind("127.0.0.1:9090")?.run().await
}
